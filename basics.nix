{ config, pkgs, ... }:

{
  # Set your time zone.
  time.timeZone = "Europe/Amsterdam";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    wget vim rxvt_unicode
  ];

  programs.mtr.enable = true;

  boot.tmpOnTmpfs = true;
  boot.cleanTmpDir = true;

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.passwordAuthentication = false;
  services.openssh.challengeResponseAuthentication = false;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "20.03"; # Did you read the comment?

  networking.firewall.extraCommands = "
    iptables -t nat -A INPUT -j SNAT --to-source 10.23.0.0-10.23.42.255
    ip6tables -t nat -A INPUT -j SNAT --to-source fec0::acab:2342:0-fec0::acab:2342:ffff
  ";

  security.acme.email = "markus+letsencrypt@muc.ccc.de";
  security.acme.acceptTerms = true;
}
