{
  laberkiste = { ... }: {
    deployment.targetHost = "213.133.103.56";
    imports = [ ./laberkiste.nix ];
  };
  fasel = { ... }: {
    deployment.targetHost = "213.133.103.56";
    deployment.targetPort = 23022;
    imports = [ ./fasel.nix ];
  };
}
