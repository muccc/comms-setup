{ config, pkgs, ... }:

{
  users.users.markus = {
    isNormalUser = true;
    extraGroups = [ "wheel" "libvirtd" ];
    hashedPassword = "$6$P/Z6RdoNvYbxw$VVsG3EuQcaissqZcBm6IRT.t5NfT6/kTOOfigHGem70j.d.oIGzU9SJhFoUVh4FW4VAqNuoqfXGOprtjU5Jyu.";
    openssh = {
      authorizedKeys = {
        keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILUiwpETwMRVueebO8aC6fBv0uYvuByJPPnpczP8kAIP markus" ];
      };
    };
  };
  users.users.neunr = {
    isNormalUser = true;
    extraGroups = [ "wheel" "libvirtd" ];
    hashedPassword = "$6$3Ppvwt/4vrHp55xb$a/DqR2DlJJ5LzUWTvSHw3.Wo.94dZuLawaN.2rK0gvliBxe4yTEyia3XwNrXQqkRVydQRnv3nJNzRft1X809G0";
    openssh = {
      authorizedKeys = {
        keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDyWcvk8smOkAtTBI0WDw+VmiGw4jOxvCt1LsCXJMrO+ 9R" ];
      };
    };
  };
  users.users.root = {
    openssh = {
      authorizedKeys = {
        keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILUiwpETwMRVueebO8aC6fBv0uYvuByJPPnpczP8kAIP markus" "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDyWcvk8smOkAtTBI0WDw+VmiGw4jOxvCt1LsCXJMrO+ 9R" ];
      };
    };
  };
}
