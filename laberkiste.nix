{ config, pkgs, ... }:

{
  imports =
    [
      ./laberkiste-hardware-configuration.nix
      ./basics.nix
      ./users.nix
    ];

  boot.supportedFilesystems = [ "zfs" ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.zfs.requestEncryptionCredentials = false;

  boot.loader.grub.mirroredBoots = [
    { devices = [ "/dev/sda" ];
      path = "/boot1"; }
    { devices = [ "/dev/sdb" ];
      path = "/boot2"; }
  ];

  networking.hostId = "eb627e70";
  networking.hostName = "laberkiste";
  networking.useDHCP = false;
  networking.interfaces.enp2s0.useDHCP = true;
  networking.bridges.mumble = {
    interfaces = [];
  };
  networking.bridges.jitsi = {
    interfaces = [];
  };
  networking.interfaces.mumble = {
    ipv4 = {
      addresses = [{
        address = "10.0.0.1";
        prefixLength = 24;
      }];
    };
  };
  networking.interfaces.jitsi = {
    ipv4 = {
      addresses = [{
        address = "10.0.1.1";
        prefixLength = 24;
      }];
    };
  };

  networking.firewall.extraCommands = ''
    iptables -t nat -A PREROUTING -p tcp --dport 23022 -j DNAT --to-destination 10.0.0.2:22

    iptables -t nat -A PREROUTING -p tcp --dport 23122 -j DNAT --to-destination 10.0.1.2:22
    iptables -t nat -A PREROUTING -p tcp --dport 4443 -j DNAT --to-destination 10.0.1.2
    iptables -t nat -A PREROUTING -p udp --dport 10000:20000 -j DNAT --to-destination 10.0.1.2
  '';
  networking.firewall.allowedTCPPorts = [ 80 443 4443 64738 ];
  networking.firewall.allowedUDPPorts = [ 64738 ];
  networking.firewall.allowedUDPPortRanges = [{ from = 10000; to = 20000; }];

  networking.nat.enable = true;
  networking.nat.externalInterface = "enp2s0";
  networking.nat.internalInterfaces = [ "mumble" "jitsi" ];

  networking.interfaces.enp2s0.ipv6.addresses = [
    { address = "2a01:4f8:a0:9137::1";
      prefixLength = 64; }
  ];
  networking.defaultGateway6 = {
    address = "fe80::1";
    interface = "enp2s0";
  };

  services.nginx = {
    enable = true;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;
    virtualHosts."fasel.muc.ccc.de" = {
      listen = [
        { addr = "0.0.0.0"; port = 80; }
        { addr = "[::]"; port = 80; }
      ];
      locations."/" = {
        proxyPass = "http://10.0.0.2/";
      };
    };
    virtualHosts."fasel.fnord.group" = {
      locations."/" = {
        proxyPass = "http://10.0.0.2/";
      };
    };
    virtualHosts."video.fnord.group" = {
      listen = [
        { addr = "0.0.0.0"; port = 80; }
        { addr = "[::]"; port = 80; }
      ];
      locations."/" = {
        proxyPass = "http://10.0.1.2/";
      };
    };
    virtualHosts."webex.muc.ccc.de" = {
      listen = [
        { addr = "0.0.0.0"; port = 80; }
        { addr = "[::]"; port = 80; }
      ];
      locations."/" = {
        proxyPass = "http://10.0.1.2/";
      };
    };
    appendHttpConfig = ''
      access_log off;
    '';
    appendConfig = ''
      stream {
        map $ssl_preread_server_name $name {
            video.fnord.group webex_jitsi;
            webex.muc.ccc.de webex_jitsi;
        }

        upstream fasel_murmur {
          server 10.0.0.2:64738;
        }

        upstream webex_jitsi {
          server 10.0.1.2:443;
        }

        server {
          listen 0.0.0.0:64738;
          listen 0.0.0.0:64738 udp;
          listen [::]:64738;
          listen [::]:64738 udp;
          proxy_pass fasel_murmur;
        }

        server {
          listen 0.0.0.0:443;
          listen [::]:443;
          proxy_pass $name;
          ssl_preread on;
          proxy_buffer_size 10m;
        }

        access_log off;
      }
    '';
  };

  virtualisation.libvirtd.enable = true;
  virtualisation.libvirtd.qemuRunAsRoot = false;
}
