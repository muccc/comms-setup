{ config, pkgs, ... }:

{
  imports =
    [
      ./vserver-hardware-configuration.nix
      ./basics.nix
      ./users.nix
    ];

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/33e97930-1f40-40b2-aabb-0e4205483379";
    fsType = "ext4";
  };


  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sda";

  networking.hostName = "fasel";
  networking.useDHCP = false;
  networking.interfaces.ens3.ipv4 = { addresses = [{address = "10.0.0.2"; prefixLength=24;}];};
  networking.defaultGateway = { address = "10.0.0.1"; interface = "ens3"; };
  networking.nameservers = [ "213.133.98.98" ];

  networking.firewall.allowedTCPPorts = [ 80 config.services.murmur.port ];
  networking.firewall.allowedUDPPorts = [ config.services.murmur.port ];

  services.nginx = { 
    enable = true;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;
    virtualHosts."fasel.muc.ccc.de" = {
      default = true;
      enableACME = true;
      locations."/".return = "200 Ok";
    };
    appendHttpConfig = ''
      access_log off;
    '';
  };

  users.extraGroups.mumble.members = [ "nginx" "murmur" ];

  security.acme.certs = {
    "fasel.muc.ccc.de" = {
      group = "mumble";
      allowKeysForGroup = true;
      postRun = ''
        systemctl restart murmur
      '';
      extraDomains = { "fasel.fnord.group" = null; };
    };
  };

  services.murmur = { 
    enable = true;
    registerName = "Club Mumble";
    allowHtml = false;
    logDays = -1;
    logFile = null;
    password = ""; 
    registerHostname = "fasel.muc.ccc.de";
    welcometext = "Willkommen im µc³ Mumble.";
    sslCert = "/var/lib/acme/fasel.muc.ccc.de/fullchain.pem";
    sslKey = "/var/lib/acme/fasel.muc.ccc.de/key.pem";
    extraConfig = ''
      ice="tcp -h 127.0.0.1 -p 6502"
      autobanAttempts=0
      obfuscate=true
    '';
  };
}
